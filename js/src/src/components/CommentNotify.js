import React from 'react';

export default ({ current, initial, changeHandler, loading }) => {
  initial = initial === undefined ? window.commentsAppNotify.default : initial;
  initial = initial > 0 ? initial : 1;

  current = current === undefined ? window.commentsAppNotify.default : current;
  current = current === 0 ? -1 : current;

  const inputChangeHandler = type => {
    window.commentsAppNotify.default = type;
    changeHandler(type);
  }

  const types = [];
  for (const id in window.commentsAppNotify.types) {
    types.push(
      <label className="rc_notify_type-label">
        <input type="radio"
          checked={current === +id}
          disabled={loading}
          onChange={_ => inputChangeHandler(+id)} />
        <span className="rc_notify_type-text">{window.commentsAppNotify.types[id]}</span>
      </label>
    )
  }

  return (
    <div className="rc_notify">
      <label className="rc_notify-checkbox">
        <input type="checkbox"
          checked={current > 0}
          disabled={loading}
          onChange={_ => inputChangeHandler(current > 0 ? -1 : initial)} />
        <span className="rc_notify-text">{window.Drupal.t("Notify me when new comments are posted")}</span>
      </label>
      <div className="rc_notify_type-radio-wrapper"
        style={{ display: current > 0 && types.length > 1 ? `block` : `none` }}>
        {types}
      </div>
    </div>
  );
}