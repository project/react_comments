export default {
    // api codes
    'unpublished'         : '0',
    'published'           : '1',
    'flagged'             : '2',
    'deleted'             : '3',
    'flaggedUnpublished'  : '4',

    // comment field settings
    'anonMayNotContact'   : 0,
    'anonMayContact'      : 1,
    'anonMustContact'     : 2,
}
