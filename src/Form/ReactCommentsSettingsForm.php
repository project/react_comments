<?php

namespace Drupal\react_comments\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

class ReactCommentsSettingsForm extends ConfigFormBase  {


  const GRAVATAR_DISABLED = -1;
  const GRAVATAR_FALLBACK = 0;
  const GRAVATAR_PREFER = 1;

  public function getFormId() {
    return 'react_comments_settings';
  }

  public function getEditableConfigNames() {
    return [
      'react_comments.settings',
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('react_comments.settings');
    $form['allowed_tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed HTML Tags'),
      '#description' => $this->t('A list of html tags that will be allowed in comments.'),
      '#default_value' => $config->get('allowed_tags') ?: '<a><b><em><strong><i><p>'
    ];

    $image_styles = array_map(function($el) {
      return $el->get('label');
    }, ImageStyle::loadMultiple());

    $form['user_avatar_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('User Avatar Image Style'),
      '#description' => $this->t("Choose the image style that will get applied to your user avatars. We recommend creating an image style with scale and crop 100 x 100 if you don't already have one"),
      '#options' => $image_styles,
      '#default_value' => $config->get('user_avatar_image_style') ?: 'thumbnail'
    ];

    $form['prefer_gravatar'] = [
      '#type' => 'radios',
      '#title' => $this->t('Gravatar'),
      '#description' => $this->t('Display user avatar'),
      '#options' => [
        static::GRAVATAR_DISABLED => $this->t('Disabled'),
        static::GRAVATAR_FALLBACK => $this->t('Use as a fallback'),
        static::GRAVATAR_PREFER => $this->t('Prefer over user pictures')
      ],
      static::GRAVATAR_DISABLED => [
        '#description' => $this->t('React Comments will fully ignore Gravatar images and show only user pictures set through Drupal.')
      ],
      static::GRAVATAR_FALLBACK => [
        '#description' => $this->t('React Comments will show Gravatar images only if the comment was created by a drupal user without a user picture.')
      ],
      static::GRAVATAR_PREFER => [
        '#description' => $this->t('React Comments will show Gravatar images even if the comment was created by a drupal user with a user picture.')
      ],
      '#default_value' => $config->get('prefer_gravatar') ?: static::GRAVATAR_FALLBACK,
    ];

    $form['anon_default_avatar_fid'] = [
      '#title' => $this->t('Default Avatar for Anonymous users'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://react_comments/',
      '#default_value' => $config->get('anon_default_avatar_fid')
    ];

    $form['full_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fully delete comments'),
      '#description' => $this->t("If this case is checked, comments that are deleted through React Comments' UX will be fully deleted, and so will be their replies. If unchecked, the comments will be shown to users as deleted, but the replies will remain"),
      '#default_value' => $config->get('full_delete')
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($anon_default_avatar_fid = $form_state->getValue('anon_default_avatar_fid')) {
      $file = File::load($anon_default_avatar_fid[0]);

      $errors = [];
      $image_factory = \Drupal::service('image.factory');
      $image = $image_factory->get($file->getFileUri());
      if (!$image->isValid()) {
        $supported_extensions = $image_factory->getSupportedExtensions();
        $errors[] = t('The image file is invalid or the image type is not allowed. Allowed types: %types', [
            '%types' => implode(', ', $supported_extensions),
        ]);
      }

      if (!empty($errors)) {
        $form_state->setErrorByName('anon_default_avatar_fid', $this->t('Default Avatar for Anonymous users must be an image file.'));
      }
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('react_comments.settings');
    $config->set('allowed_tags', $form_state->getValue('allowed_tags'))
      ->set('prefer_gravatar', $form_state->getValue('prefer_gravatar'))
      ->set('user_avatar_image_style', $form_state->getValue('user_avatar_image_style'))
      ->set('anon_default_avatar_fid', $form_state->getValue('anon_default_avatar_fid'))
      ->set('full_delete', $form_state->getValue('full_delete'));

    if ($anon_default_avatar_fid = $form_state->getValue('anon_default_avatar_fid')) {
      $file = File::load($anon_default_avatar_fid[0]);
      $file->setPermanent();
      $file->save();
      $config->set('anon_default_avatar_fid', $anon_default_avatar_fid);
    }

    $config->save();

    // Most of these configs change the way comments are displayed in some way... Unfortunately we gotta clear those caches.
    Cache::invalidateTags(['react_comment_list']);

    parent::submitForm($form, $form_state);
  }
}
